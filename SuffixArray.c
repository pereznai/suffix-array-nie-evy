#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>



struct suffix{
    int ind;
    char *suff;
};
struct simple{
    char letter;
    int value;
};
int compare(const void *a, const void *b){
    struct suffix sa = *(struct suffix*) a;
    struct suffix sb = *(struct suffix*) b;
    if(strcmp(sa.suff,sb.suff)<0){
        return 1;
    }
    return 0;
}
struct suffix * suf(char *str){
    int len = strlen(str);
    struct suffix *suf = (struct suffix*) malloc ( len * sizeof ( *suf ) );
    for(int i = i; i < len; i++){
        suf[i].ind = i;
        suf[i].suff = (str+i);
    }
    return suf;
}
int * suffixArray(char *str){
    int len = strlen(str);
    struct suffix *suffixes = suf(str);
    int *sa = (int *) malloc (len * sizeof (*sa));
    qsort(suffixes,len,sizeof(struct suffix),compare);
    for(int i = 0; i < len; i++){
        sa[i] = suffixes[i].ind;
    }
    return sa;
}
int lcp(char *str1, char *str2){
    int i = 0;
    int len = fmin(strlen(str1),strlen(str2));
    char * res = (char*) malloc ( len * sizeof ( *res ) );
    while(i<len && str1[i] == str2[i]){
        res[i] = str1[i];
    }
    return strlen(res);
}
struct simple *simpleSearch(char **L, char *x){
    struct simple *res = (struct simple*) malloc ( 2 * sizeof ( *res ) );
    int d = -1,l,f = strlen(x),id;
    while(d+1<f){
        id = floor((d+f)/2);
        l=lcp(x,L[id]);
        if(l==strlen(x) && l == strlen(L[id])){
            res[0].letter = 'i';
            res[0].value = id;
            return res;
        } else if((l==strlen(L[id]))||(l!=strlen(x))){
            d = id;
        } else{
            f = id;
        }
    }
    res[0].letter = 'd';
    res[0].value = d;
    res[1].letter = 'f';
    res[1].value = f;
    return 0;
}
int main(void){
    char **Ll;
    struct suffix *aux;
    int *sufAr;
    char *x = "me mato";
    aux = suf(x);
    sufAr = suffixArray(x);
    for(int i = 0; i < strlen(x); i++){
        Ll[i] = aux[sufAr[i]].suff;
    }
}